package Email.services;

import Email.entities.Mail;
import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import freemarker.template.TemplateExceptionHandler;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;
import org.springframework.ui.freemarker.FreeMarkerTemplateUtils;

import javax.mail.*;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

@Service
public class ServiceImpl implements MailService {

    @Override
    public void sendMail(String action, Mail mail) {

        Properties javaMailProperties = new Properties();
        javaMailProperties.put("mail.smtp.host", "smtp.gmail.com");
        javaMailProperties.put("mail.smtp.port", "465");
        javaMailProperties.put("mail.smtp.auth", "true");
        javaMailProperties.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
        javaMailProperties.put("mail.smtp.starttls.enable", "true");
        javaMailProperties.put("mail.debug", "true");
        Session session = Session.getDefaultInstance(javaMailProperties,
                new javax.mail.Authenticator() {
                    protected PasswordAuthentication getPasswordAuthentication() {
                        return new PasswordAuthentication("amingoneng@gmail.com",
                                "Romaroma12");
                    }
                });

        MimeMessage message = new MimeMessage(session);
        MimeMessageHelper helper;
        try {
            helper = new MimeMessageHelper(message, true);
            helper.setTo(mail.getMailTo());
            helper.setSubject(mail.getMailSubject());
            String body = generateContentFromTemplate(action, mail);


            Multipart multiPart = new MimeMultipart("alternative");
//            MimeBodyPart textPart = new MimeBodyPart();
            MimeBodyPart htmlPart = new MimeBodyPart();
            htmlPart.setContent(body, mail.getContentType());

//            multiPart.addBodyPart(textPart); // <-- first
            multiPart.addBodyPart(htmlPart); // <-- second
            message.setContent(multiPart);
//            ADD ATTACHMENTS
//            FileSystemResource file = new FileSystemResource(new File("E:/ngantuk.jpg"));
//            helper.addAttachment("ngantuk.jpg", file);
            Transport.send(message);
        } catch (MessagingException e) {
            e.printStackTrace();
        }
    }

    private String generateContentFromTemplate(String action, Mail mail){
        Map<String,String> temp = new HashMap<>();
        temp.put("registration", "registration.ftl");
        temp.put("change_status", "test.ftl");
        try {
            StringBuilder content = new StringBuilder();
            Configuration cfg = new Configuration(Configuration.VERSION_2_3_25);
            cfg.setClassForTemplateLoading(this.getClass(), "/templates/");
            cfg.setDefaultEncoding("UTF-8");
            cfg.setTemplateExceptionHandler(TemplateExceptionHandler.RETHROW_HANDLER);
            cfg.setLogTemplateExceptions(false);
            Template t = cfg.getTemplate(temp.get(action));
            content.append(FreeMarkerTemplateUtils.processTemplateIntoString(t, mail));
            return content.toString();
        } catch (IOException | TemplateException e) {
            e.printStackTrace();
        }
        return "";
    }

}
