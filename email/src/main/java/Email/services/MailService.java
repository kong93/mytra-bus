package Email.services;

import Email.entities.Mail;

public interface MailService {
    void sendMail(String action, Mail mail);
}
