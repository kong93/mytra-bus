package Email.controllers;

import Email.entities.Mail;
import Email.services.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;

@RestController
public class APIController {

    @Autowired
    private ServiceImpl service;

    @RequestMapping(value = "/sendmsg", method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity<Map<String,String>> sendMessage(
            @RequestBody Mail mail) {
        Map<String,String> res = new HashMap<>();
        try {
            service.sendMail("change_status", mail);
            res.put("status","OK");
            res.put("msg","Message sent");
            return new ResponseEntity<>(res, HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            res.put("status","Failed");
            res.put("msg","Something went wrong");
            return new ResponseEntity<>(res, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}